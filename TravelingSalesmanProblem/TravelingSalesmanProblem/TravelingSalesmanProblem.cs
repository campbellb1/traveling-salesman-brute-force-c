﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Traveling Salesman Problem
//	File Name:		TravelingSalesmanProblem.cs
//	Description:    This program finds the shortest path to travel between all given points and return to the beginning.
//	Author:			Brandon Campbell, campbellb1@.etsu.edu
//	Created:		9/11/2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelingSalesmanProblem
{

    /// <summary>
    /// This class is responsible for all functions and variables of the Traveling Salesman Problem.
    /// </summary>
    class TravelingSalesmanDriver
    {
        /// <summary>
        /// The main class here 
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {

            Stopwatch sw = Stopwatch.StartNew(); //creates sw and starts stopwatch



            int numentries = Int32.Parse(Console.ReadLine());   //read in the first number, which tells how many xy coordinates there will be
            int readContentCounter = 0;                         //initialize the variable that will count how many entries have been read.
            string[] arrayOfInput = new string[(numentries)];   //this array will keep all of the raw coordinate data.
            int[] pointsIndexOnly = new int[numentries + 1];    //this array will keep a numbered array for the eventual tracking of permutations. 
            string[,] arrayOfPoints = new string[numentries + 1, 2];          //initialize the list that will hold the raw string records per line                             
            int permutationsCount = numentries + 1;             //this integer will hold the number of permutations eventually.
            double shortestDistance = 0;                        //this double will hold what will eventually hold the shortest route found.


            double[,] distances = new double[numentries + 1, numentries + 1];   //This grid-array of doubles will hold the distances calculated between each point.


            //Reads in all of the entries in the input and stores them in "records"
            for (int i = 0; i < numentries; i++)
            {
                arrayOfInput[i] = (Console.ReadLine());
            }

            //numbers the pointsIndexOnly array
            for (int i = 0; i < numentries + 1; i++)
            {
                pointsIndexOnly[i] = i;
            }

            List<string> tempCoordinateList = new List<string>(); //initialize the list that will temporarily hold coordinates.

            //initialize point 0 to 0,0
            arrayOfPoints[0, 0] = "0";
            arrayOfPoints[0, 1] = "0";

            //This for loop will turn the inidividual lines into two records: one for the x and one for the y. Then it will add that to the listOfLists
            for (int i = 1; i < arrayOfInput.Length + 1; i++)
            {
                tempCoordinateList = arrayOfInput[i - 1].ToString().Split(' ').ToList();    //split up the space-separated x,y coordinates
                arrayOfPoints[i, 0] = tempCoordinateList[0];    //
                arrayOfPoints[i, 1] = tempCoordinateList[1];
            }

            //Find the total number of permutations that will exist.
            for (int i = 1; i < numentries + 1; i++)
            {
                permutationsCount = permutationsCount * i;
            }

            /*The following loops will fill up the distances grid-array using the distance-between-two-points formula*/
            for (int i = 0; i < numentries + 1; i++)
            {
                for (int x = 0; x < numentries + 1; x++)
                {
                    if (distances[i, x] == 0) //if the distances are 0, there is a chance they have not been filled. Attempt to find the distance.
                    {
                        //perform basic 2-points distance determination.
                        double tempDistance = Math.Sqrt(((Double.Parse(arrayOfPoints[i, 0]) - Double.Parse(arrayOfPoints[x, 0])) * (Double.Parse(arrayOfPoints[i, 0]) - Double.Parse(arrayOfPoints[x, 0])) + (Double.Parse(arrayOfPoints[i, 1]) - Double.Parse(arrayOfPoints[x, 1])) * (Double.Parse(arrayOfPoints[i, 1]) - Double.Parse(arrayOfPoints[x, 1]))));
                        if (tempDistance != 0)
                        {
                            distances[i, x] = tempDistance; //put the calculated distance in the appropriate grid-slot
                        }
                    }
                }
            }

            for (int i = 1;i<pointsIndexOnly.Length - 1;i++) //loop through all permutations
            {
                shortestDistance += distances[i,i + 1]; //go ahead and instantiate the 'shortestDistance' so that it won't be zero.
            }

            for(int i = 1; i < permutationsCount; i++) //loop through all permutations
            {
                pointsIndexOnly = getNextPermutation(pointsIndexOnly, numentries + 1);

                //If the first index in the permutation is less than the final index, skip it--because it has already been done.
                if(pointsIndexOnly[0] < pointsIndexOnly[pointsIndexOnly.Length - 1])
                {
                    double testDistance = distances[0, pointsIndexOnly[0]]; //calculate the distance between point 0 and the starting point and put it into a variable to hold this test run's distance.

                    //simulate traveling among the points in the current permutation.
                    for (int x = 1; x < pointsIndexOnly.Length; x++)
                    {
                        testDistance += distances[pointsIndexOnly[x], pointsIndexOnly[x - 1]];//add the distance between two adjacent points.
                        
                       
                        //check to see if the distance so far is greater or equal to the current best. If it is, instead of continuing to run, break to save time.
                        if(testDistance >= shortestDistance)
                        {
                            break;
                        }

                        //if this run was the final run 
                        if (x == pointsIndexOnly.Length - 1)
                        {
                            testDistance += distances[pointsIndexOnly[x], 0]; //add the time it takes to travel from the final point to the beginning.
                        }

                    }
                    //If this route was shorter than any former shortest distance, replace the shortest distance with the current one.
                    if(testDistance < shortestDistance)
                    {
                        shortestDistance = testDistance;
                    }
                }
            }

            

            Console.WriteLine(Math.Round(shortestDistance, 2)); //this outputs the shortest distance (to 2 decimal places).

            sw.Stop(); // stops stopwatch
            Console.WriteLine("Time used: {0} secs", sw.Elapsed.TotalMilliseconds / 1000);  //shows how long the program took to run.

            Console.ReadLine(); //stops the program to allow for reading the stats.
            Console.ReadLine();
        }



        /// <summary>
        /// Gets the next permutation for the passed in array.
        /// </summary>
        /// <param name="array">The value.</param>
        /// <param name="numberOfPoints">The number of points.</param>
        /// <returns></returns>
        public static int[] getNextPermutation(int[] array, int numberOfPoints)
        {
            int i = numberOfPoints - 1; //this keeps track of the max index of the points.

            //so long as the i-1 array entry is greater or equal to i.
            while (array[i - 1] >= array[i])
            {
                i = i - 1; //for each loop, subtract 1 from the index "counter"
            }
           
            int x = numberOfPoints; //instantiate x to be the actual number of points
            while (array[x - 1] <= array[i - 1])
            {
                x = x - 1; //decrement 1 from x until [x-1] is less or equal to [i-1]
            }

            //swap [i-1] with [x-1]
            int k = array[i - 1]; 
            array[i - 1] = array[x - 1];
            array[x - 1] = k;

            i++; 
            x = numberOfPoints; //restore x back to the numberofpoints.

          //while i is less than x, keep swapping [i-1] with [x-1], then increment i and decrement x
            while (i < x)
            {
                int l = array[i - 1];
                array[i - 1] = array[x - 1];
                array[x - 1] = l;
                i++;
                x--;
            }

            return array; //we now have the next permutation, so return it to the Main.
        }

        
    }
}